# tcp2udp.py
TCP to UDP adapter for Android App ‘Seansor Streamer’ to forward data from accelerometer sensor\
usage:\
python3 tcp2upd.py [portin] [portout] [dst]\
i.e.\
python3 tcp2udp.py 5002 5001 192.168.0.107 

# mpu2soc.py
MPU6050 sensor adapter Raspberry to send data on some host port\
Preparation.\
Install dependencies:\
sudo apt install python3-smbus\
pip install mpu6050-raspberrypi\
Enable I2C:\
sudo raspi-config\
Select Interfacing options -> I2C, choose <Yes>\
Wire VCC, GND, SDA, SCL lines.\
For more details ref.\
https://www.instructables.com/How-to-Use-the-MPU6050-With-the-Raspberry-Pi-4/

Usage:\
python3 mpu2soc.py [port] [dst] [proto]\
i.e.\
python3 mpu2soc.py 5001 192.168.0.107 udp\
or with defaults 5001 127.0.0.1 udp\
python3 mpu2soc.py

# Acceleration calibration file
If exist calibration.json file it will be loaded with values for sensor data adjustment.\
Example of such file content:\
{ "accelerometer_offset_x": 0.0,  "accelerometer_offset_y" : 0.0,  "accelerometer_offset_z" : 0.8 }
