import serial
import json
import os.path
import socket
import sys
import time

ser = serial.Serial(port='/dev/ttyACM0', baudrate=115200, stopbits=1, timeout=None)  # open serial por
print(ser.name)         # check which port was really used

d = ""
# default settings
proto = 'udp'
port = 5000
host = '127.0.0.1'

args = len(sys.argv) - 1

pos = 1
while (args >= pos):
    print ("Parameter at position %i is %s" % (pos, sys.argv[pos]))
    if pos  == 1 :
      port = int(sys.argv[pos])
    if pos  == 2 :
      host  = sys.argv[pos]
    if pos  == 3 :
      proto = sys.argv[pos]
    pos = pos + 1

print (f"Forward mpu acc data proto '{proto}' to port {port} at '{host}'")

line = ''

if proto == 'tcp' :
    host = socket.gethostname()
    sl = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    sl.connect((host, port))

if proto == 'udp':
   sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)


while True:
	s = ser.read()
	if s < b'\x80'and s > b'\x20':
		d = d + s.decode('ascii')
	if s == b'\n' or s == b'\r':
		if len(d) > 2:
			if "$GPRMC" in d or  "$GPVTG" in d:
				print(d)
				if proto == 'tcp' :
					sl.sendall(d.encode())
				if proto == 'udp' :
					sock.sendto(d.encode(), (host, port))
				time.sleep(0.5)

		d = ""
ser.close()             # close port
