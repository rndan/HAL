import json
import os.path
import socket
import sys
import time
import mpu6050

# usage:
# python3 mpu2soc.py [port] [host] [proto]
# python3 mpu2soc.py 5001 192.168.0.106 udp
# or with default args
# python mpu2soc.py

# default settings
proto = 'udp'
port = 5001
host = '127.0.0.1'
sensor = mpu6050.mpu6050(0x68)

avgx=0.0
avgy=0.0
avgz=0.0
axi=0.0
ayi=0.0
azi=0.0
count=1.0

cf = 'calibration.json'
xoff = 0
yoff = 0
zoff = 0.9

if os.path.isfile(cf) :
  jf = open(cf)
  jd = json.load(jf)
  xoff = jd["accelerometer_offset_x"]
  yoff = jd["accelerometer_offset_y"]
  zoff = jd["accelerometer_offset_z"]
else :
  print ("Calibration file " + cf + " not found")
print ("Calibration: x=" + str(xoff) + " y=" + str(yoff) + " z=" + str(zoff))

args = len(sys.argv) - 1

pos = 1
while (args >= pos):
    print ("Parameter at position %i is %s" % (pos, sys.argv[pos]))
    if pos  == 1 :
      port = int(sys.argv[pos])
    if pos  == 2 :
      host  = sys.argv[pos]
    if pos  == 3 :
      proto = sys.argv[pos]
    pos = pos + 1

print (f"Forward mpu acc data proto '{proto}' to port {port} at '{host}'")

line = ''

if proto == 'tcp' :
    host = socket.gethostname()
    sl = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    sl.connect((host, port))

if proto == 'udp':
   sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

while True:
  try:
      # print (line)
      # send json with acc data like {"accelerometer":{"value":[0.823822,8.935364,4.063919]}
        acc = sensor.get_accel_data()
        x = round(float(acc['x'] + xoff),6)
        y = round(float(acc['y'] + yoff),6)
        z = round(float(acc['z'] + zoff),6)
        axi = axi + x
        ayi = ayi + y
        azi = azi + z
        avgx = axi/count
        avgy = ayi/count
        avgz = azi/count
        count = count + 1.0
#        print (str(round(float(avgx),4)) + " " + str(round(float(avgy),4)) + " " + str(round(float(avgz),4)))
#        print (x,y,z)
        pl = {}
        dt = {}
        vl = [x,y,z]
        dt ["value"] = vl
        pl["accelerometer"] = dt
        js = json.dumps(pl)
        print (js)
        if proto == 'tcp' :
          sl.sendall(js.encode())
        if proto == 'udp' :
          sock.sendto(js.encode(), (host, port))
        time.sleep(0.5)

  except KeyboardInterrupt:
    if proto == 'tcp' :
      sl.close()
    print("\nExit at Ctrl^C")
    print ("average from counts: " + str(int(count-1)))
    print (str(round(float(avgx),4)) + " " + str(round(float(avgy),4)) + " " + str(round(float(avgz),4)))
    exit(0)

