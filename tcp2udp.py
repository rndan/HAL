import json
import socket
import sys

# Usage:
# python3 tcp2upd.py [portin] [portout] [dst]
# python3 tcp2udp.py 5002 5001 192.168.0.107
# or with defaults:
# python3 tcp2udp.py

def validate (text):
    try:
        j = json.loads(text)
        return text
    except ValueError as e:
        print('invalid json: %s' % e)
        return ""

# defaults
portin  = 5002
portout = 5001
dst = '127.0.0.1'

args = len(sys.argv) - 1

pos = 1
while (args >= pos):
    print ("Parameter at position %i is %s" % (pos, sys.argv[pos]))
    if pos == 1 :
      portin = int(sys.argv[pos])
    if pos == 2 :
      portout = int(sys.argv[pos])
    if pos == 3 :
      dst = sys.argv[pos]
    pos = pos + 1

print ("valid JSONs from TCP port " + str(portin) + " are forvarded to UDP port " + str(portout) + " at host " + dst)

server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
server_address = ('0.0.0.0', portin)
server_socket.bind(server_address)
server_socket.listen(3)
client_address = 0

udp_socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

while True :
    try:
          print('tcp server listen at ' + str(portin))
          connection, client_address = server_socket.accept()
          print ("new connection from " + str(client_address))
          addrc =  client_address[1]
          if  addrc != 0 :
            while addrc > 0:
              try:
                jmsg = connection.recv(1024)
                if len(jmsg) == 0 :
                  addrc = -1
                  break # listen again to new connection
                raw = jmsg.decode("utf-8")
                print (raw)
                jraw = validate (raw)
                if jraw.count("accelerometer") == 1 :
                    js = json.loads(jraw)
                    udp_socket.sendto(jraw.encode(), (dst, portout))

              except KeyboardInterrupt:
                server_socket.close()
                udp_server.close()
                exit(0)

    except KeyboardInterrupt:
       server_socket.close()
       udp_socket.close()
       print("\nExit at Ctrl^C")
       exit(0)
